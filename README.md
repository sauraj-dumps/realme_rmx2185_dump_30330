## sys_oplus_mssi_64_cn-user 11 RP1A.200720.011 1609743540423 release-keys
- Manufacturer: realme
- Platform: 
- Codename: RMX2185
- Brand: realme
- Flavor: cipher_RMX2185-userdebug
- Release Version: 13
- Id: TP1A.220624.021.A1
- Incremental: eng.ubuntu.20220827.090651
- Tags: release-keys
- CPU Abilist: 
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/RMX2185/RMX2185:13/TP1A.220624.021.A1/2902844826721:user/release-keys
- OTA version: 
- Branch: sys_oplus_mssi_64_cn-user-11-RP1A.200720.011-1609743540423-release-keys
- Repo: realme_rmx2185_dump_30330


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
